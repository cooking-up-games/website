import Image from 'next/image'

export default function Header() {
	return (
		<header className="z-50 md:sticky top-0 shadow-lg shadow-rosePine-base bg-rosePine-surface p-4">
			<nav className="flex mx-auto max-w-7xl items-center justify-between lg:px-8" aria-label="Global">
				<div className="flex lg:flex-1">
					<a href="/" title="Cooking Up Games">
						<span className="sr-only">Cooking Up Games</span>
						<Image
							src="/images/logo/logo-white.png"
							alt="Tailwind Logo"
							className=""
							width={120}
							height={120}
						/>
					</a>
				</div>
				<div className="flex lg:hidden mt-6">
					<div className="-my-6">
						<div className="py-6">
							<a href="/blog" className="-mx-3 block rounded-lg px-3 py-1 font-semibold leading-7 hover:bg-gray-50">Blog</a>
							<a href="/games" className="-mx-3 block rounded-lg px-3 py-1 font-semibold leading-7 hover:bg-gray-50">Games</a>
							<a href="/resources" className="-mx-3 block rounded-lg px-3 py-1 font-semibold leading-7 hover:bg-gray-50">Resources</a>
							<a href="/events" className="-mx-3 block rounded-lg px-3 py-1 font-semibold leading-7 hover:bg-gray-50">Events</a>
							<a href="/donate" className="-mx-3 block rounded-lg px-3 py-1 font-semibold leading-7 hover:bg-gray-50">Donate</a>
						</div>
					</div>
				</div>
				<div className="hidden lg:flex lg:gap-x-12">
					<a href="/blog" className="font-semibold leading-6">Blog</a>
					<a href="/games" className="font-semibold leading-6">Games</a>
					<a href="/resources" className="font-semibold leading-6">Resources</a>
					<a href="/events" className="font-semibold leading-6">Events</a>
					<a href="/donate" className="font-semibold leading-6">Donate</a>
				</div>
			</nav>
		</header >
	)
}
