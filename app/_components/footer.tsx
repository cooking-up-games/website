import Link from 'next/link'

export default function Footer() {
	const date = new Date();

	return (
		<footer className="bg-rosePine-base py-6">
			<section className="flex mx-auto max-w-7xl items-center justify-between lg:px-8">
				<div className="flex lg:flex-1">
					<p>&copy;{date.getFullYear()} Cooking Up Games</p>
				</div>
				<div className="flex flex-col text-right">
					<p className="block">
						This website is licensed under
						<Link href="https://creativecommons.org/publicdomain/zero/1.0/" className="ms-1 underline text-rosePine-rose">Creative Commons Zero</Link>
					</p>
					<p className="block">
						Feel free to view/use our source code on
						<Link href="https://gitlab.com/cooking-up-games/website" className="ms-1 underline text-rosePine-rose">GitLab</Link>
					</p>
				</div>
			</section>
		</footer >
	)
}

