import classnames from 'classnames';

export default function H1({ children, color }: Readonly<{ children: React.ReactNode, color?: string }>) {
	let classes = classnames(
		'text-5xl',
		'-rotate-2',
		'text-rosePine-surface',
		`bg-${color || "rosePine-rose"}`,
		'mb-6',
		'inline-block',
		'p-3'
	)

	return (
		<h1 className={classes}>{children}</h1>
	)
}
