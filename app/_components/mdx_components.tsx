import { DetailedHTMLProps, AnchorHTMLAttributes, HTMLAttributes } from 'react'

export default function MDXComponents() {
	return {
		h2: (props: DetailedHTMLProps<HTMLAttributes<HTMLElement>, HTMLElement>) =>
			<h2 className="text-2xl mb-2 text-rosePine-gold">{props.children}</h2>,

		strong: (props: DetailedHTMLProps<HTMLAttributes<HTMLElement>, HTMLElement>) =>
			<strong className="font-bold text-lg">{props.children}</strong>,

		p: (props: DetailedHTMLProps<HTMLAttributes<HTMLElement>, HTMLElement>) =>
			<p className="my-2">{props.children}</p>,

		em: (props: DetailedHTMLProps<HTMLAttributes<HTMLElement>, HTMLElement>) =>
			<em className="italic">{props.children}</em>,

		a: (props: DetailedHTMLProps<AnchorHTMLAttributes<HTMLAnchorElement>, HTMLAnchorElement>) =>
			<a className="text-rosePine-rose underline" href={props.href} target="_blank"> {props.children}</a>,

		code: (props: DetailedHTMLProps<HTMLAttributes<HTMLElement>, HTMLElement>) => {
			return (
				<code className="text-sm text-rosePine-foam px-3 py-2 bg-rosePine-base">
					{props.children}
				</code>
			)
		},

		pre: (props: DetailedHTMLProps<HTMLAttributes<HTMLElement>, HTMLElement>) =>
			<pre className="bg-rosePine-base p-4">{props.children}</pre>,

		ul: (props: DetailedHTMLProps<HTMLAttributes<HTMLElement>, HTMLElement>) =>
			<ul className="list-disc">{props.children}</ul>,

		li: (props: DetailedHTMLProps<HTMLAttributes<HTMLElement>, HTMLElement>) =>
			<li className="list-item">{props.children}</li>,
	}
}
