import fs from "fs"
import path from "path"
import { compileMDX } from "next-mdx-remote/rsc"
import type { Metadata } from "next";
import H1 from '@/app/_components/h1'

export const metadata: Metadata = {
	title: "Blog | Cooking Up Games",
	description: "Cooking Up Games is a space for game developers, artists, musicians, entrepreneurs, writers, and all other creatives. Currently hosted by The Biz Foundry in Cookeville Tennessee in association with Tennessee Tech University.",
};

interface front {
	title: string
	description: string
	slug: string
	date: string
	author: string
}

async function getAllPosts() {
	const postFilePaths = fs.readdirSync("data/_posts").filter((postFilePath) => {
		return path.extname(postFilePath).toLowerCase() === ".mdx"
	})

	const allPostPromises =
		postFilePaths.map(async postFilePath => {
			const postFile = fs.readFileSync(`data/_posts/${postFilePath}`)

			const mdxSource = await compileMDX<front>({ source: postFile, options: { parseFrontmatter: true } })
			return mdxSource
		})

	return Promise.all(allPostPromises)
}

export default async function Blog() {
	const postData = await getAllPosts()

	return (
		<div className="flex mx-auto max-w-7xl items-start justify-between py-4">
			<section>
				<H1>
					<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="currentColor" className="w-8 h-8 me-3 inline">
						<path fillRule="evenodd" d="M7.502 6h7.128A3.375 3.375 0 0 1 18 9.375v9.375a3 3 0 0 0 3-3V6.108c0-1.505-1.125-2.811-2.664-2.94a48.972 48.972 0 0 0-.673-.05A3 3 0 0 0 15 1.5h-1.5a3 3 0 0 0-2.663 1.618c-.225.015-.45.032-.673.05C8.662 3.295 7.554 4.542 7.502 6ZM13.5 3A1.5 1.5 0 0 0 12 4.5h4.5A1.5 1.5 0 0 0 15 3h-1.5Z" clipRule="evenodd" />
						<path fillRule="evenodd" d="M3 9.375C3 8.339 3.84 7.5 4.875 7.5h9.75c1.036 0 1.875.84 1.875 1.875v11.25c0 1.035-.84 1.875-1.875 1.875h-9.75A1.875 1.875 0 0 1 3 20.625V9.375ZM6 12a.75.75 0 0 1 .75-.75h.008a.75.75 0 0 1 .75.75v.008a.75.75 0 0 1-.75.75H6.75a.75.75 0 0 1-.75-.75V12Zm2.25 0a.75.75 0 0 1 .75-.75h3.75a.75.75 0 0 1 0 1.5H9a.75.75 0 0 1-.75-.75ZM6 15a.75.75 0 0 1 .75-.75h.008a.75.75 0 0 1 .75.75v.008a.75.75 0 0 1-.75.75H6.75a.75.75 0 0 1-.75-.75V15Zm2.25 0a.75.75 0 0 1 .75-.75h3.75a.75.75 0 0 1 0 1.5H9a.75.75 0 0 1-.75-.75ZM6 18a.75.75 0 0 1 .75-.75h.008a.75.75 0 0 1 .75.75v.008a.75.75 0 0 1-.75.75H6.75a.75.75 0 0 1-.75-.75V18Zm2.25 0a.75.75 0 0 1 .75-.75h3.75a.75.75 0 0 1 0 1.5H9a.75.75 0 0 1-.75-.75Z" clipRule="evenodd" />
					</svg>

					Recent Posts:
				</H1>

				<ul>
					{postData.map(({ frontmatter }) => (
						<li className="py-3 border-dotted border-b-2 border-b-rosePine-pine last-of-type:border-b-0" key={frontmatter.slug}>
							<h2 className="text-2xl bold"><a className="text-rosePine-rose hover:underline hover:text-rosePine-gold" href={`/blog/${frontmatter.slug}`}>{frontmatter.title}</a></h2>
							<p className="italic text-sm mb-3">By {frontmatter.author} on {frontmatter.date}</p>
							<p>{frontmatter.description}</p>
							<a className="underline text-rosePine-rose hover:text-rosePine-gold" href={`/blog/${frontmatter.slug}`}>Read More</a>
						</li>
					))}
				</ul>
			</section>
		</div >
	);
}
