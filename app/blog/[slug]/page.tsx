import fs from "fs"
import path from "path"
import { compileMDX, CompileMDXResult } from "next-mdx-remote/rsc"
import H1 from '@/app/_components/h1'
import MDXComponents from '@/app/_components/mdx_components'


export function generateStaticParams() {
	const postFilePaths = fs.readdirSync("data/_posts").filter((postFilePath) => {
		return path.extname(postFilePath).toLowerCase() === ".mdx"
	})

	return postFilePaths.map(postFilePath => {
		return { slug: postFilePath.slice(0, -4) }
	})
}

async function getPost(slug: string): Promise<CompileMDXResult<Record<string, string>>> {
	const postFile = fs.readFileSync(`data/_posts/${slug}.mdx`)

	const mdxSource = await compileMDX<{ title: string }>({
		source: postFile,
		options: { parseFrontmatter: true },
		components: MDXComponents
	})
	return mdxSource
}

export default async function PostPage({ params }: { params: { slug: string } }) {
	const { slug } = params
	const { content, frontmatter } = await getPost(slug)
	return (
		<div className="flex mx-auto max-w-7xl items-start justify-between py-4">
			<article className="">
				<H1>{frontmatter.title}</H1>
				{content}
			</article>
		</div>
	)
}
