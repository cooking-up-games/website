import type { Metadata } from "next";
import Header from '@/app/_components/header'
import Footer from '@/app/_components/footer'
import "./globals.css";

export const metadata: Metadata = {
	title: "Cooking Up Games",
	description: "Cooking Up Games is a space for game developers, artists, musicians, entrepreneurs, writers, and all other creatives. Currently hosted by The Biz Foundry in Cookeville Tennessee in association with Tennessee Tech University.",
};

export default function RootLayout({ children, }: Readonly<{ children: React.ReactNode; }>) {
	return (
		<html lang="en">
			<body>
				<main className="">
					<Header />
					<div className="bg-gradient-to-b from-rosePine-overlay to-rosePine-surface py-8 px-3">
						{children}
					</div>
					<Footer />
				</main>
			</body>
		</html>
	);
}
