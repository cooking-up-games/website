import type { Config } from "tailwindcss";

const config: Config = {
	content: [
		"./pages/**/*.{js,ts,jsx,tsx,mdx}",
		"./components/**/*.{js,ts,jsx,tsx,mdx}",
		"./app/**/*.{js,ts,jsx,tsx,mdx}",
	],
	theme: {
		extend: {
			backgroundImage: {
				"gradient-radial": "radial-gradient(var(--tw-gradient-stops))",
				"gradient-conic":
					"conic-gradient(from 180deg at 50% 50%, var(--tw-gradient-stops))",
			},
			colors: {
				rosePine: {
					base: "#232136",
					surface: "#2a273f",
					overlay: "#393552",
					muted: "#6e6a86",
					subtle: "#908caa",
					text: "#e0def4",
					love: "#eb6f92",
					gold: "#f6c177",
					rose: "#ea9a97",
					pine: "#3e8fb0",
					foam: "#9ccfd8",
					iris: "#c4a7e7",
					highlightLow: "#2a283e",
					highlightMed: "#44415a",
					highlightHigh: "#56526e",
				}
			}
		}
	},
	plugins: [],
};
export default config;
