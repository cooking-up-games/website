/** @type {import('next').NextConfig} */

const isProd = process.env.NODE_ENV === 'production';

const nextConfig = {
	output: "export",
	reactStrictMode: true,
	images: {
		unoptimized: true,
	},
	assetPrefix: isProd ? "https://cooking-up-games.gitlab.io/website/" : ""
}

export default nextConfig;
